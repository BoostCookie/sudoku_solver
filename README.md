Very small project for solving sudokus.
Build with
```
cargo build --release
```

Run example with
```
./target/release/sudoku_solver examples/example1.sudoku.txt
```
It reads in basic textfiles that are formatted like this:
```
.-----.-----.-----.
|. . 5|. . 7|. 6 .|
|8 . .|. 1 .|. 2 .|
|. . 1|. 5 .|. . 8|
:----- ----- -----:
|. 2 9|. . .|. . .|
|. 5 .|9 . 3|. 1 .|
|. . .|. . .|3 9 .|
:----- ----- -----:
|5 . .|. 4 .|2 . .|
|. 8 .|. 3 .|. . 4|
|. 4 .|1 . .|7 . .|
'-----'-----'-----'
```
