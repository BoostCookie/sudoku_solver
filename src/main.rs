use std::env;
use std::fs;
use std::io;

fn main() {
    let mut args = env::args();
    args.next();

    for arg in args {
        println!("File: {}", arg);
        let mut grid = match import_puzzle(&arg, '.', 1) {
            Ok(grid) => grid,
            Err(error) => {
                eprintln!("{}", error);
                continue;
            },
        };
        print_puzzle(&grid);
        solve(&mut grid, 0);
    }
}

fn solve(grid: &mut [i8], mut start_i: usize) {
    // find next entry with 0
    while start_i < 81 && grid[start_i] != 0 {
        start_i += 1;
    }

    // a solution was found
    if start_i == 81 {
        println!("Found solution:");
        print_puzzle(&grid);
        return;
    }

    // try out possible values
    let no_opts = invalid_options(start_i, &grid);
    for option in 1..10 as i8 {
        if (no_opts & (1 << (option - 1))) != 0 {
            continue;
        }
        grid[start_i] = option;
        solve(grid, start_i + 1);
    }

    // ran out of options
    grid[start_i] = 0;
}

// returns an int of which the last 10 bits represent the invalid options
fn invalid_options(pos_i: usize, grid: &[i8]) -> i16 {
    let mut no_opts = 0;

    fn flip_bit(no_opts: i16, num: i8) -> i16 {
        match num {
            0 => no_opts,
            _ => no_opts | (1 << (num - 1)),
        }
    }
    
    // check row 
    let row = pos_i / 9;
    let row_start = row * 9;
    for num in &grid[row_start..row_start+9] {
        no_opts = flip_bit(no_opts, *num);
    }

    // check column
    let col = pos_i % 9;
    for row_i in 0..9 {
        no_opts = flip_bit(no_opts, grid[9 * row_i + col]);
    }

    // check field
    let field_row_i = (row / 3) * 3;
    let field_col_i = (col / 3) * 3;
    for row_i in field_row_i..field_row_i+3 {
        for col_i in field_col_i..field_col_i+3 {
            no_opts = flip_bit(no_opts, grid[9 * row_i + col_i]);
        }
    }

    no_opts
}

fn import_puzzle(filename: &str, emptychar: char, mut skiprows: i8) -> Result<[i8; 81], io::Error> {
    let contents = fs::read_to_string(filename)?;
    let mut grid: [i8; 81] = [0; 81];
    let mut num_i = 0;
    for c in contents.chars() {
        if skiprows > 0 {
            if c == '\n' {
                skiprows -= 1;
            }
            continue;
        }

        let num = match c {
            '1'..='9' => c.to_digit(10).unwrap() as i8,
            a if a == emptychar => 0,
            _ => -1,
        };

        if num != -1 {
            grid[num_i] = num;
            num_i += 1;

            if num_i == 81 {
                break;
            }
        }
    }
    Ok(grid)
}

fn print_puzzle(grid: &[i8]) {
    println!(".-----.-----.-----.");
    for line_scope in 0..3 {
        for line in 0..3 {
            print!("|");
            for col_scope in 0..3 {
                let start = 27 * line_scope + 9 * line + 3 * col_scope;
                for num_i in start..start+3 {
                    match grid[num_i] {
                        0 => print!("."),
                        i => print!("{}", i),
                    }
                    if num_i != start + 2 {
                        print!(" ");
                    }
                }
                print!("|");
            }
            print!("\n");
        }
        match line_scope {
            2 => println!("'-----'-----'-----'"),
            _ => println!(":----- ----- -----:"),
        }
    }
}
