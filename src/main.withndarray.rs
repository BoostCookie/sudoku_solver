use std::env;
use std::fs;
use ndarray::{s, Array2};

fn main() {
    let mut args = env::args();
    args.next();

    for arg in args {
        println!("File: {}", arg);
        let mut grid = import_puzzle(&arg, '.', 1);
        print_puzzle(&grid);
        solve(&mut grid, 0, 0);
    }
}

fn solve(grid: &mut Array2<i8>, mut row_i: usize, mut col_i: usize) {
    // find next entry with 0
    while row_i != 9 && grid[[row_i, col_i]] != 0 {
        let (new_row_i, new_col_i) = next_index(row_i, col_i);
        row_i = new_row_i;
        col_i = new_col_i;
    }

    // a solution was found
    if row_i == 9 {
        println!("Found solution:");
        print_puzzle(&grid);
        return;
    }

    // try out possible values
    let no_opts = invalid_options(row_i, col_i, &grid);
    for option in 1..10 {
        if (no_opts & (1 << (option - 1))) != 0 {
            continue;
        }
        grid[[row_i, col_i]] = option;
        let (next_row_i, next_col_i) = next_index(row_i, col_i);
        solve(grid, next_row_i, next_col_i);
    }

    // ran out of options
    grid[[row_i, col_i]] = 0;
}

// returns an int of which the last 10 bits represent the invalid options
fn invalid_options(row_i: usize, col_i: usize, grid: &Array2<i8>) -> i16 {
    let mut no_opts = 0;

    fn flip_bit(no_opts: &mut i16, num: i8) {
        if num == 0 {
            return;
        }
        *no_opts |= 1 << (num -1)
    }
    
    // check line
    for num in grid.row(row_i).iter() {
        flip_bit(&mut no_opts, *num);
    }

    // check column
    for num in grid.column(col_i).iter() {
        flip_bit(&mut no_opts, *num);
    }

    // check field
    let field_row_i = (row_i / 3) * 3;
    let field_col_i = (col_i / 3) * 3;
    for num in grid.slice(s![field_row_i..field_row_i+3, field_col_i..field_col_i+3]).iter() {
        flip_bit(&mut no_opts, *num);
    }

    no_opts
}

fn import_puzzle(filename: &str, emptychar: char, mut skiprows: i8) -> Array2<i8> {
    let contents = fs::read_to_string(filename).unwrap();
    let mut grid = Array2::<i8>::zeros((9, 9));
    let mut row_i = 0;
    let mut col_i = 0;
    for c in contents.chars() {
        if skiprows > 0 {
            if c == '\n' {
                skiprows -= 1;
            }
            continue;
        }

        let num = match c {
            '1'..='9' => c.to_digit(10).unwrap() as i8,
            a if a == emptychar => 0,
            _ => continue,
        };

        grid[[row_i, col_i]] = num;

        let (new_row_i, new_col_i) = next_index(row_i, col_i);
        row_i = new_row_i;
        col_i = new_col_i;

        if row_i == 9 {
            // we're full
            break;
        }
    }
    grid
}

fn next_index(mut row_i: usize, mut col_i: usize) -> (usize, usize) {
    if col_i == 8 {
        row_i += 1;
        col_i = 0;
    } else {
        col_i += 1;
    }
    (row_i, col_i)
}

fn print_puzzle(grid: &Array2<i8>) {
    println!(".-----.-----.-----.");
    for line_scope in 0..3 {
        for line in 0..3 {
            print!("|");
            for col_scope in 0..3 {
                for col in 0..3 {
                    match grid[[3 * line_scope + line, 3 * col_scope + col]] {
                        0 => print!("."),
                        i => print!("{}", i),
                    }
                    if col != 2 {
                        print!(" ");
                    }
                }
                print!("|");
            }
            print!("\n");
        }
        match line_scope {
            2 => println!("'-----'-----'-----'"),
            _ => println!(":----- ----- -----:"),
        }
    }
}
